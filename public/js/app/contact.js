var app = angular.module('contact', [
    'ngAnimate'
]);

app.controller("ContactCtrl", function($scope) {
    $scope.enableServices = function(event) {
        $scope.checkbox = !$scope.checkbox;
        event.stopPropagation();
    };
    $scope.disableServices = function() {
        $scope.checkbox = false;
    };
    $scope.tickService = function(event, key) {
        $scope.services[key].checked = !$scope.services[key].checked;
        $scope.selected = [];
        for (var element in $scope.services) {
            if ($scope.services[element].checked)
                $scope.selected.push($scope.services[element].name);
        }
        $scope.value = $scope.selected.join(",");
        $scope.selected = $scope.selected.join(", ");
        $scope.label = $scope.selected.length > 25 ? $scope.selected.substring(0, 25) + "..." : $scope.selected;
        event.stopPropagation();
    };
    $scope.sendForm = function(event) {
        document.forms[0].submit();
    };
});

window.onload = function() {
    var graph = bodymovin.loadAnimation({
        container: document.getElementById("contact"),
        renderer: "svg",
        autoplay: true,
        prerender: true,
        autoloadSegments: false,
        frameRate: 60,
        loop: true,
        animationData: data
    });
};