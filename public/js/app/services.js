var app = angular.module('services', [
    'ngAnimate',
    'ui.router'
]);

app.constant('services', {
    domestic: {
        fullname: "Domestic growth audit & Consultancy",
        prev: "consultancy",
        link: "domestic-growth",
        next: "international-growth",
        cta: "/contact",
        name: "domestic growth",
        image: "https://res.cloudinary.com/drrgcbwl7/image/upload/v1518813188/services1BIG_jeczqh.jpg",
        body: "Even with many years of digital marketing knowledge, it can be difficult to know exactly just how hard each digital channel is working for you. Each marketing agency can have different ideas on strategy and, besides, it would take you hours to analyse the level of market and in-house data that is required to thoroughly understand real performance.\n \nION’s team of digital marketing experts with 19+ years of agency and client-side knowledge to give you the real in-depth truths on your domestic performance in your home market: How is each channel performing versus your brand’s potential? Where are the exact opportunities in growing market share in under 90 days? And how should your analytics change going forwards to attribute the correct value across your digital channels?\n\n Our auditing and consultancy will give you the increased information needed to better lead in-house and agency teams, whilst growing results now, not later. It’s like hiring global leaders in each digital area for a month, without the ongoing costs!"
    },
    international: {
        fullname: "International growth audit & Consultancy",
        prev: "domestic-growth",
        link: "international-growth",
        next: "audits",
        cta: "/contact",
        name: "international growth",
        image: "https://res.cloudinary.com/drrgcbwl7/image/upload/v1519149510/services2BIG_num49d.jpg",
        body: "Our International Growth Audit & Consultancy is similar to our Domestic Growth Audit & Consultancy, with one crucial difference: we don’t just analyse your home-market, but we also analyse the opportunities that you are not taking in other markets (countries where you are investing, and countries where you are not). For example, one of our current US-based clients expanded into Spain following our guided strategy, achieving an ROI and increased global digital market share within 60 days.\nOur team of Consultants are some of the most experienced digital marketers in the world, leading a team of multi-lingual, multi-location analysts. The knowledge and information that you will gain during this fast process will put the power in your hands to better manage global agencies and impact results now, not later.  It’s like hiring global leaders in each digital area for a month, without the ongoing costs!"
    },
    audit: {
        fullname: "Audits",
        prev: "international-growth",
        link: "audits",
        next: "consultancy",
        cta: "/contact",
        name: "audits",
        image: "https://res.cloudinary.com/drrgcbwl7/image/upload/v1518813188/services3BIG_xbhgpy.jpg",
        bullets: [
            "AFFILIATE MARKETING AUDIT",
            "ANALYTICS AUDIT",
            "BRAND REPUTATION AUDIT",
            "BRANDING & DESIGN AUDIT",
            "COMPETITOR AUDIT",
            "CONTENT MARKETING AUDIT",
            "CREATIVE STRATEGY AUDIT",
            "DIGITAL MARKETING STRATEGY AUDIT",
            "DIGITAL MONETISATION AUDIT",
            "EMAIL MARKETING AUDIT",
            "INFLUENCER MARKETING AUDIT",
            "MEDIA PLANNING \& BUYING AUDIT",
            "PPC AUDIT",
            "SEO AUDIT",
            "SOCIAL MEDIA AUDIT",
            "TECH AUDIT",
            "USER EXPERIENCE AUDIT",
        ],
        body: "Audits in digital marketing have a bad reputation. Too often they involve some technology scanning your website and stating the obvious. ION’s audits are very different. Aided by in-house technology (soon available for licensing), our analysis is human-led by some of the most experienced people in the worldwide digital marketing industry. Let us show you, within weeks, the immediate situation across your digital channels and highlight the opportunities being lost...right now.\n\n Request information on any of the following audit areas and we will show you new strategies to grow new segments of customers:"
    },
    integrated: {
        fullname: "Integrated Digital Marketing Services",
        prev: "international-growth",
        link: "integrated-digital-marketing-services",
        next: "consultancy",
        cta: "/contact",
        name: "integrated",
        image: "https://res.cloudinary.com/keystone-demo/image/upload/c_fit,h_600,q_80/ubspxa8k3eissbbrixow",
        bullets: [],
        body: "At ION, we believe in the sum of all parts to drive greater value. Your digital marketing strategy and optimisation should be integrated by real channel specialists; across SEO, Social Media, Paid Media, Content, Influencer Marketing, Design, UX, Creative, Development, PR, Affiliate, Email and Analytics.\n\nOur leadership team has been working in the digital marketing industry since its inception, which means that you can be confident that your brand will receive the correct strategic guidance from day one. We apply a senior level of consultancy unmatched during the first months of client partnerships that ensures key results can be obtained quickly. We find that this approach creates trust and improved ways of working in the shortest possible time.\n\n If you are looking to understand how each channel can work together to drive higher market share and revenue, we can show you how our team’s approach has added millions of dollars to brands’ revenues within 12 month periods."
    },
    consultancy: {
        fullname: "Consultancy",
        prev: "integrated",
        link: "consultancy",
        next: "domestic-growth",
        cta: "/contact",
        name: "Consultancy",
        bullets: [
            "AFFILIATE MARKETING CONSULTANCY",
            "ANALYTICS CONSULTANCY",
            "BRAND REPUTATION CONSULTANCY",
            "BRANDING & DESIGN CONSULTANCY",
            "CONTENT MARKETING CONSULTANCY",
            "CREATIVE STRATEGY CONSULTANCY",
            "DIGITAL COPYWRITING TRAINING",
            "DIGITAL MARKETING STRATEGY CONSULTANCY",
            "DIGITAL MARKETING TRAINING",
            "DIGITAL MONETISATION CONSULTANCY",
            "EMAIL MARKETING CONSULTANCY",
            "INFLUENCER MARKETING CONSULTANCY",
            "MARKET RESEARCH CONSULTANCY",
            "MEDIA PLANNING & BUYING CONSULTANCY",
            "PPC CONSULTANCY",
            "SEO CONSULTANCY",
            "SALES STRATEGY CONSULTANCY",
            "SOCIAL MEDIA CONSULTANCY",
            "SOCIAL MEDIA TRAINING",
            "TECH CONSULTANCY",
            "USER EXPERIENCE CONSULTANCY",
        ],
        image: "https://res.cloudinary.com/drrgcbwl7/image/upload/v1518813188/services4BIG_uznsbe.jpg",
        body: "Whilst our audits show you the gaps and opportunities surrounding your existing strategy in each area of your digital marketing, our Consultancies show you how to use that information to produce immediate increases in market share via improved strategy. ION’s Consultants have been developing insight-led strategies for both local and major global brands since 1998. We understand the complexities of each different sector and we have the experience to show you practical examples and integrated, creative changes that will produce the difference that will drive results. It’s one thing to have the information from audits, but it’s another to have the deep specialist experience to put that information to its highest effect. By hiring our global experts for such a short time, you will save yourself potentially months of continuing with a stable but less than optimal strategy."
    }
})

app.constant('template', "/js/app/templates/service.html")

app.config(function ($stateProvider, $urlRouterProvider, services, template) {
    $urlRouterProvider.otherwise("/domestic-growth");
    $stateProvider
        .state(services.domestic.link, {
            url: '/' + services.domestic.link,
            params: services.domestic,
            templateUrl: template,
            controller: "ViewCtrl"
        })
        .state(services.international.link, {
            url: '/' + services.international.link,
            params: services.international,
            templateUrl: template,
            controller: "ViewCtrl"
        })
        .state(services.audit.link, {
            url: '/' + services.audit.link,
            params: services.audit,
            templateUrl: template,
            controller: "ViewCtrl"
        })
        .state(services.integrated.link, {
            url: '/' + services.integrated.link,
            params: services.integrated,
            templateUrl: template,
            controller: "ViewCtrl"
        })
        .state(services.consultancy.link, {
            url: '/' + services.consultancy.link,
            params: services.consultancy,
            templateUrl: template,
            controller: "ViewCtrl"
        });
});

app.controller("ViewCtrl", function ($scope, $stateParams) {
    $scope.service = $stateParams;
});

app.controller("ServiceCtrl", function ($scope, $state, services) {
    $scope.prev = function (prev) {
        $state.go(prev);
    };
    $scope.next = function (next) {
        $state.go(next);
    };
});