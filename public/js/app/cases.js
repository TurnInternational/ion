var app = angular.module('cases', [
    'ngAnimate',
    'ui.router',
    'ngSanitize'
]);

var hammer = new Hammer(document.getElementById("cases"));

app.constant('cases', {
    ibis: {
        fullname: "ibis hotels",
        subtitle: "Booking market share in mexico",
        link: "ibis-hotels",
        name: "ibis",
        image: "https://res.cloudinary.com/drrgcbwl7/image/upload/v1518806216/ibis_bxme4b.jpg",
        body: `Now a Facebook page boasting 1.6m followers, ION created the launch strategy for ibis Hotels to enter the Mexican Social Media market. One year later, ION completed an in-depth <a href="http://www.theideaofnow.com/services#!/consultancy">consultancy</a> to take the results even further, producing another boost in results over the subsequent 90 days:
                <br />
                <ul>
                <li>1,220% increase in booking revenue from Facebook.</li>
                <li>34.6% increase in the engagement rate of social media posts.</li>
                <li>130.2% increase in people sharing social media content, showing an improvement in both creative strategy and targeting of content.</li>
                <li>83.8% increase in people commenting on social media posts, highlighting the increased relevance of the amended content strategy.</li>
                <li>23.4% decrease in the number of people reached but a 3% increase in the number of people engaging with social media, again showing a significant improvement in targeting. </li>
                </ul>
                If your brand has social media networks that need a 90 day boost from specialist consultancy, we would love to show you similar results this year.`,
        prev: "aig",
        next: "russell-bedford",
        service: {
            key: "domestic-growth",
            anchor_text: "Domestic Growth Audit & Consultancy"
        }
    },
    russell: {
        fullname: "Russell Bedford",
        subtitle: "Cost efficiency for global accountancy firm",
        link: "russell-bedford",
        name: "Russell Bedford",
        image: "https://res.cloudinary.com/drrgcbwl7/image/upload/v1519402099/case-russell3_obxgfq.jpg",
        body: `Russell Bedford Mexico’s in-house marketing team had already established a successful PPC campaign at the moment of engaging ION to review and restructure their Google AdWords account. From February to April, ION’s 19 years of PPC experience for major global brands inspired the following results:
        <br />
        <ul>
          <li>37% decrease in cost per conversion</li>
          <li>20% increase in conversion rate</li>
          <li>72.2% increase in average time on website per user</li>
          <li>8.6% increase in the number of pages viewed per user</li>
          <li>4.4% decrease in bounce rate</li>
        </ul>
        Following these excellent results, ION was also engaged to hold seminars on digital marketing at Russell Bedford’s AGM, to help educate all International offices on efficiently growing digital market share. This was also followed up by a series of highly-reviewed webinars delivered to marketing departments in over 30 countries. Please get in touch if your brand could use our specialist experience to increase cost efficiencies and train your in-house teams`,
        prev: "ibis-hotels",
        next: "aig",
        service: {
            key: "international-growth",
            anchor_text: "International Growth Audit & Consultancy"
        }
    },
    aig: {
        fullname: "AIG",
        subtitle: "90 days to drive ppc",
        link: "aig",
        name: "AIG",
        image: "https://res.cloudinary.com/drrgcbwl7/image/upload/v1519149510/case-aig2_bn8eam.jpg",
        body: `AIG hired ION’s <a href="http://www.theideaofnow.com/services#!/consultancy">PPC Consultancy service</a> at a time where the company was desperate for the digital channel to function. AIG’s ex-digital agency had failed to create a profitable car insurance campaign and the digital channel was set to be deactivated. ION had exactly 60 days to turn things around. Here is what happened:
        <br>
        <b>Month 1:</b>
        <ul>
          <li>47.7% decrease in cost per conversion</li>
          <li>34.4% increase in conversion rate</li>
          <li>30% decrease in cost per clicks</li>
        </ul>
        <b>Month 2:</b>
        <ul>
          <li>43% more conversions than forecasted</li>
          <li>43% lower cost per conversion than forecasted</li>
          <li>35% higher conversion rate than forecasted </li>
        </ul>ION was consequently asked to stay with AIG throughout to offer guidance on both PPC and SEO. ION also had the privilege of being told by AIG’s Head of Marketing for Latin America that we were the first company to achieve a profitable car insurance campaign in the first month of activity.`,
        prev: "russell-bedford",
        next: "ibis-hotels",
        service: {
            key: "audit",
            anchor_text: "PPC Audit"
        }
    },
})

app.constant('template', "/js/app/templates/case.html")

app.config(function($stateProvider, $urlRouterProvider, cases, template) {
    $urlRouterProvider.otherwise("/ibis-hotels");
    $stateProvider
        .state(cases.ibis.link, {
            url: '/' + cases.ibis.link,
            params: cases.ibis,
            templateUrl: template,
            controller: "CaseCtrl"
        })
        .state(cases.aig.link, {
            url: '/' + cases.aig.link,
            params: cases.aig,
            templateUrl: template,
            controller: "CaseCtrl"
        })
        .state(cases.russell.link, {
            url: '/' + cases.russell.link,
            params: cases.russell,
            templateUrl: template,
            controller: "CaseCtrl"
        })
});

app.controller("CaseCtrl", function($scope, $stateParams, $state, cases) {
    $scope.case = $stateParams;
    // hammer.on('swiperight', function(event) {
    //     $state.go($scope.case.prev);
    // });
    // hammer.on('swipeleft', function(event) {
    //     $state.go($scope.case.next);
    // });
});