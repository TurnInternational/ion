var keystone = require('keystone');
var Enquiry = keystone.list('Enquiry');

exports = module.exports = function(req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;

    // locals.section is used to set the currently selected
    // item in the header navigation.
    locals.section = 'about';
    locals.formData = req.body || {};
    view.on('post', {
        action: 'leadership'
    }, function(next) {
        var newEnquiry = new Enquiry.model();
        var updater = newEnquiry.getUpdateHandler(req);
        if (req.body.message.length == 0) {
            req.body.message = "No messaged left, just contact details";
        }
        updater.process(req.body, {
            flashErrors: true,
            fields: 'name, company, website, email, message',
            errorMessage: 'There was a problem submitting your enquiry:',
        }, function(err) {
            if (err) {
                locals.validationErrors = err.errors;
            } else {
                locals.enquirySubmitted = true;
            }
            next();
        });
    });
    // Render the view
    view.render('team');
};