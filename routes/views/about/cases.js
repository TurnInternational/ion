var keystone = require('keystone');

exports = module.exports = function(req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;

    // locals.section is used to set the currently selected
    // item in the header navigation.
    locals.section = 'case-studies';
    locals.services = [{
        link: "domestic-growth",
        name: "Domestic growth",
    }, {
        link: "international-growth",
        name: "international growth",
    }, {
        link: "audits",
        name: "Audits",
    }, {
        link: "consultancy",
        name: "Consultancy",
    }];

    // Render the view
    view.render('cases');
};