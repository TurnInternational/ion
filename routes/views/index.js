var keystone = require('keystone');

exports = module.exports = function(req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;

    // locals.section is used to set the currently selected
    // item in the header navigation.
    locals.section = 'home';
    locals.title = 'ION Marketing: UK & International SEO & Digital Media Agency';
    locals.description = 'Global integrated digital marketing agency. Specialists in SEO, PPC, Social Media, Paid Media, Creative, Web Design. Offices: UK, US, Mexico & South America.';

    // Render the view
    view.render('index');
};