var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Service Model
 * =============
 */

var Service = new keystone.List('Service', {});

Service.add({
    name: { type: String, required: true },
    shortname: { type: String },
    key: { type: String },
    image: { type: Types.CloudinaryImage },
    body: { type: Types.Textarea }
});

Service.register();