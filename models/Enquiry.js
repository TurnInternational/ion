var keystone = require('keystone');
var Types = keystone.Field.Types;
var countries = require("../utils/countries")
    /**
     * Enquiry Model
     * =============
     */

var Enquiry = new keystone.List('Enquiry', {
    nocreate: true,
    noedit: true,
});

Enquiry.add({
    name: {
        type: Types.Name,
        required: true
    },
    company: {
        type: Types.Name,
        required: true
    },
    country: {
        type: Types.Select,
        options: countries.list
    },
    website: {
        type: Types.Url
    },
    email: {
        type: Types.Email,
        required: true
    },
    phone: {
        type: String
    },
    services: {
        type: Types.TextArray
    },
    message: {
        type: Types.Textarea
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
});

Enquiry.schema.pre('save', function(next) {
    this.wasNew = this.isNew;
    next();
});

Enquiry.schema.post('save', function() {
    if (this.wasNew) {
        this.sendNotificationEmail();
    }
});

Enquiry.schema.methods.sendNotificationEmail = function(callback) {
    console.log('------------------------------------');
    console.log("sending email");
    console.log('------------------------------------');
    if (typeof callback !== 'function') {
        callback = function(err) {
            if (err) {
                console.error('There was an error sending the notification email:', err);
            }
        };
    }

    if (!process.env.MAILGUN_API_KEY || !process.env.MAILGUN_DOMAIN) {
        console.log('Unable to send email - no mailgun credentials provided');
        return callback(new Error('could not find mailgun credentials'));
    }

    var enquiry = this;
    var brand = keystone.get('brand');

    keystone.list('Y').model.find().where('isAdmin', true).exec(function(err, admins) {
        if (err) return callback(err);
        new keystone.Email({
            templateName: 'enquiry-notification',
            transport: 'mailgun',
        }).send({
            to: admins,
            from: {
                name: 'ION',
                email: 'contact@ion.com',
            },
            subject: 'New Enquiry for ION',
            enquiry: enquiry,
            brand: brand,
        }, callback);
    });
};

Enquiry.defaultSort = '-createdAt';
Enquiry.defaultColumns = 'name, email, enquiryType, createdAt';
Enquiry.register();